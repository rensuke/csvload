var midiName = [];	//曲名リスト収納用配列変数初期化
var midiUrl = "music/";
var rand;	//乱数収納変数
var midiPlayTitle;	//曲名収納用
var mpt;
var desc;	//http表示イントロ収納用
var playNumber = [];	//再生数収納用
var qn = 1;	//出題数変数

function getList(){
	var dataText = document.getElementById("dialogue_data").textContent;

	var tmp = dataText.split(/\r\n?|"\n/);	//一行ごとに分けて格納
	tmp.map(function(str) {return str.trim();})	//要素前後のスペース除去
	tmp.filter(function(str) {return str !== "";})	//空文字列要素の除去
	for(var i = 0; i < tmp.length; ++i){
		midiName[i] = tmp[i].split(",");	//各行をカンマで分けて格納
	}
}


function musicStart(){
	var mFunc = 0;
	while (mFunc == 0){
		rand = parseInt(Math.random()*midiName.length);
		var result = playNumber.indexOf(rand);
		if(result == (-1)){
			playNumber.push(rand);
			mFunc = 1;
		}
		else if(playNumber.length == midiName.length){
			mFunc = 2;
		}
	}
	if(mFunc == 1){
		mpt = midiName[rand];
		midiPlayTitle = midiUrl + mpt + ".mid";
		// midiPlayStart(midiPlayTitle);
		desc = "曲名：" + midiName[rand][0] + "\nイントロ：" + midiName[rand][1];
		document.getElementById("textDesc").innerHTML = desc;
	}
	else if(mFunc == 2){
		document.getElementById("midiArea").innerHTML = "<b>★終了★</b>";
	}
}

getList();	//ファイル呼び出し関数を起動